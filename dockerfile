# Alpine because it's lighter
FROM openjdk:8-jdk-alpine
MAINTAINER Yuri Cavalcante <yuri.cavalcante@wipro.com>

# Set ENV variables
ENV PORT=8081

# Add JAR file and run it as entrypoint
ADD target/product-price-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

# Expose the port
EXPOSE 8081