package B10.AL20113763.implementation.meru.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import B10.AL20113763.implementation.meru.data.Price;
import B10.AL20113763.implementation.meru.dto.ProductPriceRequest;
import B10.AL20113763.implementation.meru.dto.ProductPriceResponse;
import B10.AL20113763.implementation.meru.repository.IPriceRepository;

@RunWith(MockitoJUnitRunner.class)
@DataJpaTest
public class TestPriceService {
	
	@InjectMocks
	PriceService service;
	
	@Mock
	IPriceRepository repository;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getCurrentPriceByProductId() {
		
		List<Price> list = new ArrayList<Price>();
		Price priceOne = new Price(1L, "1", LocalDate.of(2020, Month.JANUARY, 01), LocalDate.of(2020, Month.FEBRUARY, 29), new BigDecimal(100.0));
		Price priceTwo = new Price(2L, "1", LocalDate.of(2020, Month.MARCH, 01), LocalDate.of(9999, Month.DECEMBER, 31), new BigDecimal(99.99));
		list.add(priceOne);
		list.add(priceTwo);
		
		when(repository.findCurrentPriceByProductId("1", LocalDate.now())).thenReturn(priceTwo);
		
		ProductPriceResponse price = service.findCurrentPriceByProductId("1");
		
		assertEquals(new BigDecimal(99.99), price.getPrice());		
	}
	
	@Test
	public void getHistoryPriceByProductId() {

		List<Price> list = new ArrayList<Price>();
		Price priceOne = new Price(1L, "1", LocalDate.of(2020, Month.JANUARY, 01), LocalDate.of(2020, Month.FEBRUARY, 29), new BigDecimal(100.0));
		Price priceTwo = new Price(1L, "1", LocalDate.of(2020, Month.MARCH, 01), LocalDate.of(9999, Month.DECEMBER, 31), new BigDecimal(99.99));
		list.add(priceOne);
		list.add(priceTwo);

		when(repository.findPriceHistoryByProductId("1")).thenReturn(list);
		
		List<Price> history = repository.findPriceHistoryByProductId("1");
		
		assertEquals(2, history.size());
		verify(repository, times(1)).findPriceHistoryByProductId("1");
	}
	
	@Test
	public void saveNewProductPrice() {
		
		ProductPriceRequest request = new ProductPriceRequest("1", LocalDate.of(2020, Month.JANUARY, 01), new BigDecimal(100.0));
		
		when(repository.save(Mockito.any(Price.class))).thenReturn(new Price(1L, "1", LocalDate.of(2020, Month.JANUARY, 01), LocalDate.now(), new BigDecimal(100.0)));
		
		Price createdPrice = service.saveNewProductPrice(request);
		
		assertEquals(request.getDateFrom(), createdPrice.getDateFrom());
	}

}
