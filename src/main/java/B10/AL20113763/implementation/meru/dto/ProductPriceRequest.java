package B10.AL20113763.implementation.meru.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ProductPriceRequest {

	private String productId;
	private LocalDate dateFrom;
	private BigDecimal price;
	
	public ProductPriceRequest(String productId, LocalDate dateFrom, BigDecimal price) {
		this.productId = productId;
		this.dateFrom = dateFrom;
		this.price = price;
	}

	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
