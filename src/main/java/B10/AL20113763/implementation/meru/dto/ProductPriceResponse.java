package B10.AL20113763.implementation.meru.dto;

import java.math.BigDecimal;

public class ProductPriceResponse {

	private BigDecimal price;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}	
}
