package B10.AL20113763.implementation.meru.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import B10.AL20113763.implementation.meru.data.Price;

@Repository
public interface IPriceRepository extends JpaRepository<Price, Long> {

	@Query("SELECT p FROM Price p WHERE p.productId = :productId AND :actualDate BETWEEN p.dateFrom AND p.dateTo OR p.dateTo IS NULL")
	Price findCurrentPriceByProductId(@Param("productId") String productId, @Param("actualDate") LocalDate date);

	@Query("SELECT p FROM Price p WHERE p.productId = :productId")
	List<Price> findPriceHistoryByProductId(@Param("productId") String productId);
	
}
