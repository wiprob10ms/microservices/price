package B10.AL20113763.implementation.meru.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import B10.AL20113763.implementation.meru.data.Price;
import B10.AL20113763.implementation.meru.dto.ProductPriceRequest;
import B10.AL20113763.implementation.meru.dto.ProductPriceResponse;
import B10.AL20113763.implementation.meru.service.PriceService;

@RestController
@RequestMapping("/prices")
public class PriceController {

	@Autowired
	PriceService service;
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<ProductPriceResponse> findCurrentPriceByProductId(@PathVariable("id") String id) {
		return ResponseEntity.ok(service.findCurrentPriceByProductId(id));
	}
	
	@GetMapping(value = "/history/{id}")
	public ResponseEntity<List<Price>> findPriceHistoryByProductId(@PathVariable("id") String id) {
		return ResponseEntity.ok(service.findPriceHistoryByProductId(id));
	}
	
	@PostMapping()
	public ResponseEntity<Price> saveNewProductPrice(@RequestBody ProductPriceRequest request) {
		return ResponseEntity.ok(service.saveNewProductPrice(request));
	}
}
