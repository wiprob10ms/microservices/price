package B10.AL20113763.implementation.meru.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import B10.AL20113763.implementation.meru.data.Price;
import B10.AL20113763.implementation.meru.dto.ProductPriceRequest;
import B10.AL20113763.implementation.meru.dto.ProductPriceResponse;
import B10.AL20113763.implementation.meru.exception.ResourceNotFoundException;
import B10.AL20113763.implementation.meru.repository.IPriceRepository;

@Service
public class PriceService {

	@Autowired
	IPriceRepository repository;
		
	public ProductPriceResponse findCurrentPriceByProductId(String id) {
		
		LocalDate currentDate = LocalDate.now();
		
		Price result = repository.findCurrentPriceByProductId(id, currentDate);
		
		ProductPriceResponse response = new ProductPriceResponse();
		
		if (result != null) {
			response.setPrice(result.getPrice());			
			return response;			
		}
		else {
			throw new ResourceNotFoundException("Price for this product was not found");
		}

	}
	
	public List<Price> findPriceHistoryByProductId(String id) {
		
		List<Price> history = repository.findPriceHistoryByProductId(id);
		
		if (history.size() == 0) {
			throw new ResourceNotFoundException("There is not history price for this product");
		}
		
		return history;
	}
	
	public Price saveNewProductPrice(ProductPriceRequest request) {
		
		LocalDate today = LocalDate.now();
		Price oldPrice = repository.findCurrentPriceByProductId(request.getProductId(), today);
		
		if (oldPrice != null) {
			oldPrice.setDateTo(request.getDateFrom().minusDays(1));
			repository.save(oldPrice);			
		}
				
		Price currentPrice = new Price();
		currentPrice.setDateFrom(request.getDateFrom());
		currentPrice.setDateTo(null);
		currentPrice.setPrice(request.getPrice());
		currentPrice.setProductId(request.getProductId());
		
		return repository.save(currentPrice);
	}
		
}
